# CPS 491 - Capstone II

University of Dayton

Department of Computer Science

CPS 491 - Sprint 2022

Dr. Phu Phung


# Novobi Facial Recognition


## Team members

1.  Victoria Douglas, <douglasv1@udayton.edu>
2.  Bradley Rucker, <ruckerb3@udayton.edu>
3.  Stephen Felton, <feltons2@udayton.edu>
5.  TJ Nicholson, <nicholsont1@udayton.edu>


## Company Mentors

Hoang Lam, Software Engineer/Data Scientist; 

Novobi

<https://novobi.com/>

8920 Business Park Dr  
Suite 250  
Austin TX 78759  
United States  

## Project Management Information

Management board (private access): <https://cps491facerecognition.atlassian.net/>

Source code repository (private access): <https://bitbucket.org/cps491s22-team7/cps491s22-team7.bitbucket.io/src/master/>

Project homepage (public): <https://cps491s22-team7.bitbucket.io/>

## Revision History

| Date       |   Version     |  Description |
|------------|:-------------:|-------------:|
| 1/1/2022 |  0.0          | Initial Draft   |
| 1/17/2022 |  0.1          | End of Sprint 0   |

# Overview

Creating a security camera system that leverages facial recognition AI and a database of authorized users to check whether the people showing up on the security camera feed are authorized.


# Project Context and Scope

Security cameras, especially for homes/smart home systems, are becoming more common, but one feature that these cameras fail to leverage is facial recognition. Facial recognition has the advantage of allowing a user to monitor how many people are coming/going on their property and whether or not they are recognized. The motivation for this project is to leverage facial recognition within an application that uses a camera to then give the user peace of mind by having information about the people on their property. 

# System Analysis

## High-level Requirements

1.  Process live stream video
2.  Integrate facial recognition framework and AI with video
3.  Web admin website for creating and managing users (recognized faces)
4.  Integration with messaging apps to send alert notifications

## Use Cases

### High Level Use Case Overview
![Overview Architecture](https://cps491s22-team7.bitbucket.io/imgs/overviewv1.png "Overview Architecture")

Figure 1. - Overview of the proposed facial recognition security camera system.

### Registration
![Registration](https://cps491s22-team7.bitbucket.io/imgs/UC-register-1.png "Registration")

### Login
![Login](https://cps491s22-team7.bitbucket.io/imgs/UC-login-1.png "Login")

### User Assigns Recognized Face
![User Assigns Recognized Face](https://cps491s22-team7.bitbucket.io/imgs/UC-assign-face-1.png "User Assigns Recognized Face")

### SMS/WhatsApp Alert for Stranger/Friendly Face
![Alerts](https://cps491s22-team7.bitbucket.io/imgs/UC-alerts-1.png "Alerts")

# System Design

_(Start from Sprint 1, keep updating)_

## Use-Case Realization

## Database 

## User Interface


# Implementation

_(Start from Sprint 1, keep updating. However, it is important to prepare the technology from Sprint 0)_

For each new sprint cycle, update the implementation of your system (break it down into subsections). It is helpful if you can include some code snippets to illustrate the implementation

Specify the development approach of your team, including programming languages, database, development, testing, and deployment environments.

## Technology

1.  Web framework: Django
2.  Database: PostgreSQL
3.  Live Video and Facial Recognition Framework: Python (Deepface AI framework)
4.  Messaging apps: leverage APIs of Whatsapp, Slack, etc

## Deployment

Will be deployed using Docker. Likely deployed on Heroku, but still to be determined.

# Impacts

Describe impacts of the technologies that your team decided you use in this project, i.e., the arguments that your team use these technologies.

# Software Process Management

We will follow the Scrum approach including weekly sprints and monthy releases. Team meetings will occur once a day and meetings with the product owner will occur once a week. The schedule and sprint cycles for Spring 2022 are as follows. 

![Spring 2022 Timeline](https://cps491s22-team7.bitbucket.io/imgs/course-timeline.png "Spring 2022 Timeline")


Sprint Timeline Structure (so far); 

![Spring 2022 Timeline on Jira](https://cps491s22-team7.bitbucket.io/imgs/gantt-chart-1.png "Spring 2022 Timeline on Jira")



## Scrum process

### Sprint 0

Duration: 01/01/2022-01/17/2022

#### Completed Tasks: 

1. Team website creation
2. Planning sprints/timeline and designating tasks
3. Initial setup (repository, workspace, libraries, etc)
4. README creation and updating
5. Setting up routine of weekly meetings/contacting Hoang

#### Contributions: 

(# commits excluded from Sprint 0 b/c work done primarily outside of repository)
1.  Tori, N/A commits, 10 hours, contributed in setting up meetings/jira board, scheduling, contacting Hoang, workspace setup, README
2.  Bradley, N/A commits, 10 hours, contributed in created/tested process for setting up Python workspace, and helped other team members set their workspaces up, set up jira workspace, README
3.  Stephen, N/A commits, 10 hours, contributed in organizing meetings, workspace setup, jira board management, README
4.  TJ, N/A commits, 10 hours, contributed in team website creation, workspace setup, jira board management, README creation and updating

#### Sprint 0 Retrospective:


| Good     |   Could have been better    |  How to improve?  |
|----------|:---------------------------:|------------------:|
|   We started our work early, consistent meetings during the week, contact with Hoang, helping each other in getting set up showed great teamwork      |     Team meeting attendance, should submit earlier                       |      Consistent communication in our groupchat           |


### Sprint x

Duration: MM/DD/YYYY-MM/DD/YYYY

#### Completed Tasks: 

1. Task 1
2. Task 2
3. ...

#### Contributions: 

1.  Member 1, x commits, y hours, contributed in xxx
2.  Member 2, x commits, y hours, contributed in xxx
3.  Member 3, x commits, y hours, contributed in xxx
4.  Member 4, x commits, y hours, contributed in xxx

#### Sprint Retrospection: 

| Good     |   Could have been better    |  How to improve?  |
|----------|:---------------------------:|------------------:|
|          |                             |                   |


# User guide/Demo

Write as a demo with screenshots and as a guide for users to use your system.

(Start from Sprint 1, keep updating)

# Acknowledgments 

A special thank you to Novobi and Hoang Lam for sponsoring this project and helping mentor us along the way.